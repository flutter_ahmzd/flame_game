import 'dart:ui';
import 'dart:math';
import 'dart:async';

import 'package:flame/flame.dart';
import 'package:flame/position.dart';
import 'package:flutter/material.dart';
import 'package:flame/flare_animation.dart';
import 'package:flame_game/constants/player_state.dart';

class Player {
  PlayerState playerState;
  final double maxHealth;
  FlareAnimation player;
  final Size screenSize;
  Rect playerRect;
  double health;
  double width;
  bool loaded;
  double y;
  double x;

  Player(this.screenSize)
      : loaded = false,
        maxHealth = 300,
        playerState = PlayerState.IDLE {
    width = min(screenSize.width, screenSize.height) / 5;
    y = (screenSize.height - width) / 2;
    x = (screenSize.width - width) / 2;

    health = maxHealth;

    playerRect = Rect.fromLTWH(x, y, width, width);

    _initPlayer();
  }

  void _initPlayer() async {
    player = await FlareAnimation.load('assets/flare/spaceship.flr');

    player.updateAnimation('idle');
    player.height = width;
    player.width = width;

    loaded = true;
  }

  void startGame() {
    player.updateAnimation('start');

    health = maxHealth;

    playerState = PlayerState.STARTING;

    Timer(Duration(seconds: 1), () => Flame.audio.play('takeoff.mp3'));

    Timer(Duration(seconds: 4), () {
      player.updateAnimation('fly');

      playerState = PlayerState.FLYING;
    });
  }

  Future endGame() async {
    player.updateAnimation('end');

    playerState = PlayerState.ENDING;

    Flame.audio.play('landing.mp3');

    await Future.delayed(Duration(seconds: 4), () {
      player.updateAnimation('idle');

      playerState = PlayerState.IDLE;
    });
  }

  Position get center => Position(x + width / 2, y + width / 2);

  void render(Canvas c) {
    if (!loaded) return;

    if (playerState == PlayerState.FLYING && health != maxHealth) {
      final Rect healthRect = Rect.fromLTWH(x, y - 6, width, 3);
      final Rect remainingHealthRect =
          Rect.fromLTWH(x, y - 6, width * (health / maxHealth), 3);
      c.drawRect(healthRect, Paint()..color = Colors.red);
      c.drawRect(remainingHealthRect, Paint()..color = Colors.green);
    }

    player.render(
      c,
      x: x,
      y: y,
    );
  }

  void update(double t) {
    if (loaded) player.update(t);
  }
}
