import 'dart:math';
import 'dart:ui';

import 'package:flame/flame.dart';
import 'package:flame/position.dart';
import 'package:flame_game/app/enemy.dart';
import 'package:flame_game/app/game.dart';
import 'package:flutter/material.dart';

class Missile {
  final Game game;
  bool shouldDestroy;
  final double radius;
  Offset currentOffset;
  double pixelPerSecond;
  final Size screenSize;
  final Offset startOffset;
  final Offset targetOffset;

  Missile(
    this.game,
    this.startOffset,
    this.targetOffset,
    this.pixelPerSecond,
    this.screenSize,
  )   : currentOffset = startOffset,
        shouldDestroy = false,
        radius = 4 {
    Flame.audio.play('laser.mp3');
  }

  void render(Canvas c) {
    Iterable<Enemy> hitEnemies = game.enemies.where((enemy) =>
        enemy.health > 0 &&
        enemy.center.distance(this.center) <= (enemy.width / 2));
    if (hitEnemies.isNotEmpty) {
      hitEnemies.first.health--;
      shouldDestroy = true;
    }

    if ((currentOffset.dx < -radius || currentOffset.dx > screenSize.width) ||
        (currentOffset.dy < -radius || currentOffset.dy > screenSize.height))
      shouldDestroy = true;

    if (!shouldDestroy)
      c.drawCircle(
        currentOffset,
        radius,
        Paint()
          ..color = Colors.red
          ..style = PaintingStyle.fill,
      );
  }

  Position get center =>
      Position(currentOffset.dx + radius / 2, currentOffset.dy + radius / 2);

  void update(double t) {
    Offset distance = targetOffset - startOffset;

    double dx = pixelPerSecond * cos(distance.direction) * t;
    double dy = pixelPerSecond * sin(distance.direction) * t;

    currentOffset = currentOffset.translate(dx, dy);
  }
}
