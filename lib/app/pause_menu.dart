import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flame_game/app/game.dart';

class PauseText {
  final Game game;
  TextPainter tp;

  PauseText(this.game) {
    buildText();
  }

  void buildText() {
    final String text =
        'Paused\n\nTouch Back Button\nTo Continue!\n\nTouch Screen To Exit!';
    final TextSpan span = TextSpan(
      text: text,
      style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
    );
    tp = TextPainter(
      text: span,
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );
    tp.layout();
  }

  void render(Canvas c) {
    if (game.isPaused) {
      c.drawPaint(Paint()..color = Colors.grey.withOpacity(.7));

      tp.paint(
        c,
        Offset((game.size.width - tp.width) / 2,
            (game.size.height - tp.height) / 2),
      );
    }
  }
}
