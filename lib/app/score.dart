import 'dart:ui';

import 'package:flutter/painting.dart';

class Score {
  final String score;
  final Size screenSize;

  Score(this.score, this.screenSize);

  void render(Canvas c) {
    TextSpan span = TextSpan(
      text: 'score: $score',
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    );
    TextPainter tp = TextPainter(
      text: span,
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );
    tp.layout();
    tp.paint(
        c, Offset((screenSize.width - tp.width) / 2, screenSize.height / 10));
  }
}
