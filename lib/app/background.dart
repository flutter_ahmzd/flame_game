import 'dart:ui';

import 'package:flame/sprite.dart';

class Background {
  final Sprite background;
  final Size screenSize;

  Background(this.screenSize)
      : background = Sprite('background.jpg');

  void render(Canvas c) {
    final Rect bgRect =
        Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    background.renderRect(c, bgRect);
  }
}
