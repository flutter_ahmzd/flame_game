import 'dart:ui';
import 'dart:async';

import 'package:flame/util.dart';
import 'package:flame/flame.dart';
import 'package:flutter/material.dart';
import 'package:flame/game.dart' as Base;
import 'package:flame_game/app/enemy.dart';
import 'package:flame_game/app/score.dart';
import 'package:flame_game/app/player.dart';
import 'package:flame_game/app/missile.dart';
import 'package:flame_game/pages/intro.dart';
import 'package:flame_game/app/background.dart';
import 'package:flame_game/app/pause_menu.dart';
import 'package:flame_game/app/enemy_spawner.dart';
import 'package:flame_game/constants/game_state.dart';
import 'package:flame_game/constants/enemy_state.dart';
import 'package:flame_game/constants/player_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Game extends Base.Game {
  GlobalKey<IntroPageState> introState = GlobalKey();
  final void Function(VoidCallback) setState;
  VoidCallback _onInitializeCompleteListener;
  GameState gameState = GameState.INTRO;
  final List<Missile> missiles = [];
  final List<Enemy> enemies = [];
  bool isNewHighScore = false;
  EnemySpawner enemySpawner;
  bool isPaused = false;
  Background background;
  SharedPreferences db;
  IntroPage introPage;
  PauseText pauseText;
  bool loaded = false;
  double opacity = 0;
  final Util util;
  Player player;
  int highscore;
  Size size;
  int score;

  Game(this.util, this.setState) {
    initialize();
  }

  void loadGamePage() async {
    setState(() => gameState = GameState.MAIN);

    await Future.delayed(Duration(milliseconds: 100));

    setState(() => opacity = 1);

    Flame.bgm.play('main.mp3');

    loaded = true;
  }

  void initialize() async {
    introPage = IntroPage(this, key: introState);

    db = await SharedPreferences.getInstance();
    resize(await util.initialDimensions());
    enemySpawner = EnemySpawner(this);
    background = Background(size);
    pauseText = PauseText(this);
    player = Player(size);

    highscore = db.getString('highscore') == null
        ? 0
        : int.parse(db.getString('highscore'));

    _onInitializeCompleteListener();
    _onInitializeCompleteListener = null;
  }

  Future startGame() async {
    if (player.playerState != PlayerState.IDLE) return;

    setState(() {
      gameState = GameState.PLAYING;
    });

    score = 0;
    isNewHighScore = false;

    Flame.bgm.stop();

    player.startGame();
  }

  Future endGame() async {
    gameState = GameState.MAIN;
    missiles.clear();

    if (highscore < score) {
      await db.setString('highscore', '$score');
      isNewHighScore = true;
      highscore = score;
    }

    await player.endGame();

    enemies.clear();

    Flame.bgm.play('main.mp3');

    setState(() {});
  }

  void setOnInitializeCompleteListner(VoidCallback func) {
    _onInitializeCompleteListener = func;
  }

  @override
  void render(Canvas c) {
    if (!loaded) return;

    background.render(c);

    missiles.forEach((missile) => missile.render(c));

    enemies.forEach((enemy) => enemy.render(c));

    player.render(c);

    if (gameState == GameState.PLAYING) {
      Score('$score', size).render(c);
    }

    pauseText.render(c);
  }

  @override
  void update(double t) {
    if (!loaded) return;
    if (isPaused) return;

    missiles.removeWhere((missile) => missile.shouldDestroy);

    enemies.removeWhere((enemy) => enemy.enemyState == EnemyState.DEAD);

    missiles.forEach((missile) => missile.update(t));

    enemies.forEach((enemy) => enemy.update(t));

    player.update(t);

    enemySpawner.update(t);

    if (gameState == GameState.PLAYING && player.health <= 0) endGame();
  }

  @override
  void resize(Size size) {
    this.size = size;
  }

  void onTapDown(TapDownDetails d) {
    // TODO: complete onTouch!
    if (isPaused) {
      isPaused = false;
      endGame();
      return;
    }
    if (player.playerState == PlayerState.FLYING) {
      if (Rect.fromLTWH(
        player.x + player.width / 5,
        player.y + player.width / 5,
        player.width * .6,
        player.width * .6,
      ).contains(d.globalPosition))
        print(enemies.length);
      else
        missiles.add(Missile(
          this,
          player.center.toOffset(),
          d.globalPosition,
          player.width * 2,
          size,
        ));
    }
  }
}
