import 'dart:ui';
import 'dart:math';

import 'package:flame/flame.dart';
import 'package:flame/position.dart';
import 'package:flutter/material.dart';
import 'package:flame/spritesheet.dart';
import 'package:flame_game/app/game.dart';
import 'package:flame/animation.dart' as fa;
import 'package:flame/flare_animation.dart';
import 'package:flame_game/constants/enemy_state.dart';

class Enemy {
  final Random random = Random();
  final double damagePerSecond;
  fa.Animation explosion;
  EnemyState enemyState;
  final Size screenSize;
  FlareAnimation enemy;
  final Game game;
  double width;
  bool loaded;
  int health;
  double y;
  double x;

  Enemy(this.game, this.screenSize, this.damagePerSecond)
      : loaded = false,
        health = 3,
        enemyState = EnemyState.WALKING {
    width = min(screenSize.width, screenSize.height) / 7;

    _setXY();

    _spawnEnemy();
  }

  void _setXY() {
    final int location = random.nextInt(4) + 1;
    switch (location) {
      case 1:
        x = random.nextDouble() * (screenSize.width + (width * 2)) - width;
        y = -width;
        break;
      case 2:
        x = screenSize.width + width;
        y = random.nextDouble() * (screenSize.height + (width * 2)) - width;
        break;
      case 3:
        x = random.nextDouble() * (screenSize.width + (width * 2)) - width;
        y = screenSize.height + width;
        break;
      case 4:
        x = -width;
        y = random.nextDouble() * (screenSize.height + (width * 2)) - width;
        break;
      default:
        x = -width;
        y = -width;
    }
  }

  void _spawnEnemy() async {
    enemy = await FlareAnimation.load('assets/flare/spaceship.flr');

    enemy.updateAnimation('fly');
    enemy.height = width;
    enemy.width = width;

    explosion = SpriteSheet(
      imageName: 'explosion.png',
      textureHeight: 138,
      textureWidth: 138,
      columns: 24,
      rows: 1,
    ).createAnimation(
      0,
      stepTime: 1 / 18,
      loop: false,
    );

    loaded = true;
  }

  void die() {
    game.score++;

    enemyState = EnemyState.DYING;

    Iterable<Enemy> hitEnemies = game.enemies
        .where((enemy) => enemy.center.distance(this.center) <= width);
    hitEnemies.forEach((enemy) => enemy.health -= 3);

    Flame.audio.play('blast.mp3', volume: .4);
  }

  Position get center => Position(x + width / 2, y + width / 2);

  Color getEnemyColor() {
    switch (health) {
      case 3:
        return Colors.green;
      case 2:
        return Colors.orange;
      case 1:
        return Colors.red;
      default:
        return Colors.transparent;
    }
  }

  void render(Canvas c) {
    if (!loaded) return;

    if (health < 1 &&
        enemyState != EnemyState.DYING &&
        enemyState != EnemyState.DEAD) die();

    if (enemyState == EnemyState.DYING && explosion.done())
      enemyState = EnemyState.DEAD;

    if (enemyState != EnemyState.DEAD) {
      final Rect healthRect = Rect.fromLTWH(x, y - 6, width, 2);
      c.drawRect(healthRect, Paint()..color = getEnemyColor());
    }

    if (enemyState == EnemyState.DYING) {
      explosion.getSprite().renderPosition(
            c,
            Position.fromSize(Size(x, y)),
            size: Position.fromSize(Size(width, width)),
          );
    } else if (enemyState != EnemyState.DEAD) {
      enemy.render(
        c,
        x: x,
        y: y,
      );
    }
  }

  void update(double t) {
    if (!loaded) return;

    if (enemyState != EnemyState.DYING)
      enemy.update(t);
    else
      explosion.update(t);

    if (enemyState == EnemyState.DEAD || enemyState == EnemyState.DYING) return;

    if ((game.player.center.toOffset() - center.toOffset()).distance <=
        ((game.player.width + width) / 2)) {
      if (!game.player.health.isNegative)
        game.player.health -= damagePerSecond * t;
      return;
    }

    final Offset distance = game.player.center.toOffset() - center.toOffset();

    final double speedScale =
        ((game.score ?? 0).toDouble() / 120).clamp(1, 2).toDouble();
    final double dx = width * cos(distance.direction) * t * speedScale;
    final double dy = width * sin(distance.direction) * t * speedScale;

    x += dx;
    y += dy;
  }
}
