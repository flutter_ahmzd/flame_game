import 'package:flame_game/app/game.dart';
import 'package:flame_game/app/enemy.dart';
import 'package:flame_game/constants/player_state.dart';

class EnemySpawner {
  final Game game;
  int maxEnemies;

  EnemySpawner(this.game);

  void update(double t) {
    maxEnemies = ((game.score ?? 0) ~/ 10).clamp(5, 12);

    if (game.player.playerState != PlayerState.FLYING) return;
    if (game.enemies.length < maxEnemies)
      game.enemies.add(Enemy(game, game.size, game.player.maxHealth / 10));
  }
}
