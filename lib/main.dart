import 'package:flame/util.dart';
import 'package:flutter/material.dart';
import 'package:flame_game/pages/main_page.dart';

void main() async {
  Util util = Util();

  runApp(MyApp(util));

  await util.fullScreen();
  await util.setPortraitUpOnly();
}

class MyApp extends StatelessWidget {
  final Util util;

  MyApp(this.util);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flame Game',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      home: MainPage(util),
    );
  }
}
