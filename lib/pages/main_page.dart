import 'package:flame/util.dart';
import 'package:flame/flame.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flame_game/app/game.dart';
import 'package:flame_game/constants/game_state.dart';
import 'package:flame_game/constants/player_state.dart';

// import 'package:flame_gamepad/flame_gamepad.dart';

class MainPage extends StatefulWidget {
  final Util util;

  MainPage(this.util, {Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with WidgetsBindingObserver {
  Game game;

  @override
  void initState() {
    game = Game(widget.util, setState)
      ..setOnInitializeCompleteListner(
        () => setState(() {}),
      );

    WidgetsBinding.instance.addObserver(this);
    super.initState();

    TapGestureRecognizer tapper = TapGestureRecognizer();
    tapper.onTapDown = game.onTapDown;
    widget.util.addGestureRecognizer(tapper);

    // var gamePadController = new FlameGamepad();
    // gamePadController.setListener((String evtType, String key) {
    //   print(evtType);
    //   print(key);
    // }); // TODO: gamepad event listen!
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (game.gameState == GameState.PLAYING) {
      switch (state) {
        case AppLifecycleState.resumed:
          break;
        default:
          game.isPaused = true;
          break;
      }
    } else if (game.gameState == GameState.INTRO) {
      switch (state) {
        case AppLifecycleState.resumed:
          Flame.bgm.resume();
          game.introState.currentState.resumeIntro();
          break;
        default:
          Flame.bgm.pause();
          game.introState.currentState.pauseIntro();
      }
    } else if (game.gameState == GameState.MAIN)
      switch (state) {
        case AppLifecycleState.resumed:
          Flame.bgm.resume();
          break;
        default:
          Flame.bgm.pause();
      }
  }

  Future<bool> onWillPop() async {
    if (game.gameState == GameState.PLAYING) {
      if (game.player.playerState != PlayerState.FLYING) return false;
      game.isPaused = !game.isPaused;
    } // TODO: pause manu!
    else {
      bool isInDebug = false;
      assert(isInDebug = true);
      if (isInDebug) {
        await game.db.remove("highscore");
        game.highscore = 0;
        setState(() {});
      } // TODO: for debug porposes!
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: body,
        backgroundColor: Colors.black,
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
      ),
    );
  }

  Widget get body {
    Widget res;
    switch (game.gameState) {
      case GameState.INTRO:
        res = introPageWidget;
        break;
      case GameState.MAIN:
        res = mainPageWidget;
        break;
      case GameState.PLAYING:
        res = Stack(
          fit: StackFit.expand,
          children: <Widget>[
            game.widget,
          ],
        );
        break;
      case GameState.DEAD:
      default:
        res = Container();
        // TODO: dead
        break;
    }
    return res;
  }

  Widget get introPageWidget => game.introPage;

  Widget get mainPageWidget => AnimatedOpacity(
        duration: Duration(seconds: 1),
        opacity: game.opacity,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            game.widget,
            Positioned(
              left: 0,
              right: 0,
              top: 120,
              child: Align(
                child: RaisedButton(
                  elevation: 0,
                  onPressed: null,
                  shape: StadiumBorder(),
                  child: Text("Flame Game!"),
                  disabledTextColor: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                ),
              ),
            ),
            if ((game.highscore ?? 0) != 0)
              Positioned(
                left: 0,
                right: 0,
                top: 190,
                child: Align(
                  child: RaisedButton(
                    elevation: 0,
                    onPressed: null,
                    shape: StadiumBorder(),
                    disabledTextColor: Colors.white,
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      '${game.isNewHighScore ? 'New ' : ''}High Score: ${game.highscore}',
                      textScaleFactor: 1.5,
                    ),
                  ),
                ),
              ),
            if (game.score != null && !game.isNewHighScore)
              Positioned(
                left: 0,
                right: 0,
                top: 235,
                child: Align(
                  child: RaisedButton(
                    elevation: 0,
                    onPressed: null,
                    shape: StadiumBorder(),
                    disabledTextColor: Colors.white,
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Your Score: ${game.score}',
                      textScaleFactor: 1.5,
                    ),
                  ),
                ),
              ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 42,
              child: Align(
                child: Material(
                  shape: StadiumBorder(),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(16),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      child: Text("start!", textScaleFactor: 1.2),
                    ),
                    enableFeedback: false,
                    onTap: game.highscore == null ? null : game.startGame,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
