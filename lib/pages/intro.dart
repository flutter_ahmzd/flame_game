import 'package:flame/flame.dart';
import 'package:flutter/material.dart';
import 'package:flame_game/app/game.dart';

class IntroPage extends StatefulWidget {
  final Game game;

  IntroPage(this.game, {Key key}) : super(key: key);

  @override
  IntroPageState createState() => IntroPageState();
}

class IntroPageState extends State<IntroPage>
    with SingleTickerProviderStateMixin {
  double duration = 12;
  double elapsedTime = 0;
  bool isIntroEnded = false;
  bool areAssetsLoaded = false;
  AnimationController animation;
  ColorTween backgroundColor =
      ColorTween(begin: Colors.black, end: Colors.lightBlue);

  loadAssets() async {
    await Flame.audio.loadAll([
      'main.mp3',
      'intro.mp3',
      'blast.mp3',
      'laser.mp3',
      'takeoff.mp3',
      'landing.mp3',
      'dead_enemy.mp3',
    ]);

    await Flame.images.loadAll([
      'qdl.png',
      'background.jpg',
    ]);

    if (isIntroEnded)
      loadMainPage();
    else
      areAssetsLoaded = true;
  }

  void resumeIntro() {
    animation.forward(from: elapsedTime);
  }

  void pauseIntro() {
    elapsedTime = animation.value;
    animation.stop();
  }

  loadMainPage() async {
    Flame.bgm.stop();

    await Future.delayed(Duration(milliseconds: 500));

    widget.game.loadGamePage();
  }

  double get currentFraction {
    final double amount = animation.value;

    if (amount < 2) return amount / 2;
    if (amount > (duration - 2)) return (duration - amount) / 2;
    return 1;
  }

  @override
  void initState() {
    super.initState();

    assert(duration > 4, 'duration must be equal to or greater than 4!');

    loadAssets();

    Flame.bgm.play('intro.mp3');

    animation = AnimationController(
      vsync: this,
      lowerBound: 0,
      upperBound: duration,
      duration: Duration(seconds: duration.toInt()),
    )
      ..addStatusListener((state) {
        if (state != AnimationStatus.completed) return;
        if (areAssetsLoaded)
          loadMainPage();
        else {
          Flame.bgm.stop();
          isIntroEnded = true;
        }
      })
      ..forward();
  }

  @override
  void dispose() {
    animation.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'A Game By Snowman',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 16),
              Text('Special thanks to:'),
              FractionallySizedBox(
                widthFactor: .6,
                alignment: Alignment.center,
                child: Image.asset('assets/images/qdl.png'),
              )
            ],
          ),
        ),
        animation: animation,
        builder: (context, child) {
          return Container(
            color: backgroundColor.lerp(currentFraction),
            child: Opacity(
              child: child,
              opacity: currentFraction,
            ),
          );
        });
  }
}
